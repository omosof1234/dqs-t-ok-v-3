import {
  Divider,
  IconButton,
  InputAdornment,
  InputBase,
  InputBaseProps,
  Stack,
} from '@mui/material';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';

export default function SearchField({
  onChange,
  placeholder,
  ...rest
}: InputBaseProps) {
  const style = {
    '&.MuiInputBase-root': {
      px: '15px',
      py: '5px',
      flex: 1,
      border: 'none',
      height: '150px',
      overflow: 'hidden',
      borderRadius: 50,
      backgroundColor: 'rgba(22, 24, 35, .06)',
    },
    '&.Mui-focused': {
      backgroundColor: 'rgba(22, 24, 35, .06)',
      border: ' 1.9px solid rgba(22, 24, 35, .25)',
    },
  };

  return (
    <Stack>
      <InputBase
        sx={style}
        inputProps={{ 'aria-label': 'Search icon' }}
        onChange={onChange}
        placeholder="Rechercher des comptes et des vidéos"
        type="search"
        endAdornment={
          <InputAdornment position="end">
            <Stack
              display="flex"
              direction="row"
              width="100%"
              padding="11px 16px 11px 0px"
              margin="-12px -16px -12px 0px"
              sx={{ ':hover': { backgroundColor: 'rgba(22, 24, 35, .06)' } }}
            >
              <Divider
                orientation="vertical"
                variant="middle"
                sx={{ height: '30px' }}
              />

              <IconButton
                aria-label="Search icon"
                edge="end"
                sx={{ ':hover': { backgroundColor: 'transparent' } }}
              >
                <SearchOutlinedIcon />
              </IconButton>
            </Stack>
          </InputAdornment>
        }
      />
    </Stack>
  );
}
