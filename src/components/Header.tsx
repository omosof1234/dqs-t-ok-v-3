import { AppBar, Box, Button, Grid, Stack, Toolbar } from '@mui/material';
import SearchField from './SearchField';

import logo from '../assets/images/logo.png';
import AddIcon from '@mui/icons-material/Add';
import MoreVertIcon from '@mui/icons-material/MoreVert';

export default function Header() {
  return (
    <AppBar
      sx={{
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        backdropFilter: 'blur(10px)',
        boxShadow: '0px 1px 1px rgb(0 0 0 / 12%)',
        height: '60px',
      }}
    >
      <Toolbar disableGutters>
        <Stack
          flexGrow={1}
          px={19}
          display="flex"
          direction="row"
          justifyContent="space-around"
        >
          <Grid container spacing={2} alignItems="center">
            <Grid
              item
              xs={2}
              md={2}
              lg={4}
              display="flex"
              flexDirection={'row'}
              alignItems={'center'}
            >
              <Box
                className="logo"
                component="img"
                src={logo}
                sx={{ height: '52px' }}
                alt="Logo TikTok"
              />
            </Grid>

            <Grid item xs={8} md={5} lg={4}>
              <SearchField />
            </Grid>

            <Grid item xs={2} md={5} lg={4} display="flex" justifyContent="end">
              <Stack
                direction="row"
                justifyContent="space-between"
                spacing={1.5}
                sx={{ display: { md: 'flex', sm: 'flex', xs: 'none' } }}
              >
                <Button
                  sx={{
                    backgroundColor: '#FFFFFF',
                    border: '1px solid rgba(22, 24, 35, 0.12)',
                    borderRadius: '2px',
                    color: '#000000',
                    textTransform: 'none',
                    fontWeight: 'bold',
                  }}
                  variant="outlined"
                  startIcon={<AddIcon />}
                >
                  Téléverser
                </Button>

                <Button
                  disableElevation
                  sx={{
                    backgroundColor: 'rgba(254, 44, 85, 1) ',
                    border: 'none',
                    borderRadius: '4px',
                    color: 'rgba(255, 255, 255, 1)',
                    textTransform: 'none',
                    fontWeight: 'bold',
                    ':hover': {
                      backgroundColor: 'rgba(254, 44, 85, 1) ',
                      border: 'none',
                      borderRadius: '4px',
                      color: 'rgba(255, 255, 255, 1)',
                      textTransform: 'none',
                      fontWeight: 'bold',
                    },
                  }}
                  variant="contained"
                >
                  Connexion
                </Button>

                <Button
                  sx={{
                    '&.MuiButton-root': {
                      color: '#000000',
                    },
                    backgroundColor: 'transparent',
                    border: 'none',
                    ':hover': {
                      border: 'none',
                      backgroundColor: 'transparent',
                    },
                  }}
                  variant="outlined"
                >
                  <MoreVertIcon />
                </Button>
              </Stack>
            </Grid>
          </Grid>
        </Stack>
      </Toolbar>
    </AppBar>
  );
}
