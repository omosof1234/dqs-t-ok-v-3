import { Stack } from '@mui/material';

export default function MainContent() {
  return <Stack width="65%" bgcolor="rgba(22, 24, 35, .06)"></Stack>;
}
